import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { EcomService } from 'src/app/services/ecom.service';

@Component({
  selector: 'app-addpayment',
  templateUrl: './addpayment.component.html',
  styleUrls: ['./addpayment.component.scss']
})
export class AddpaymentComponent implements OnInit {

  constructor(private apiService: EcomService,
    private formbuilder: FormBuilder,
    private shareData: DataService,
    private router: Router) { }
  login = false;

  details: any;
  orderDetails: any;
  price: any;
  customerDetails: any;

  myForm = this.formbuilder.group({
    PaymentMode: ['', Validators.required],
    CardNumber: [''],
    CardCvv: [''],
    CardExpiry: [''],
    CardName: ['']
  });

  add() {
    if (this.login) {
      if (this.myForm.status == 'VALID') {
        this.apiService.addOrder(this.orderDetails).subscribe(response => {
          console.log(response);
          this.details = response;

          let payment = {
            OrderId: this.details.orderId,
            PaymentMode: this.myForm.value.PaymentMode,
            CardNumber: this.myForm.value.CardNumber,
            CardCvv: this.myForm.value.CardCvv,
            CardExpiry: this.myForm.value.CardExpiry,
            CardName: this.myForm.value.CardName
          }
  
          this.apiService.addPayment(payment).subscribe(response => {
            console.log(response);
          })
        })
        alert("Order placed successfully. You will recive your order in 7 business days.");
      }
      else {
        alert("Please fill all the fields to continue.");
      }
    }
    else {
      alert("Please login or Register to purchase.");
    }
  }
  ngOnInit(): void {
    let data = JSON.parse(localStorage.getItem('login') || "{}");
    let loginRole = data.loginRole;
    if (loginRole == "USER") {
      this.login = true;
    }
    this.orderDetails = this.shareData.getOrder();
    this.price = this.orderDetails.OrderPrice;
    this.customerDetails = JSON.parse(localStorage.getItem('customer') || "");

  }

}
