import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { EcomService } from 'src/app/services/ecom.service';

@Component({
  selector: 'app-addcategory',
  templateUrl: './addcategory.component.html',
  styleUrls: ['./addcategory.component.scss']
})
export class AddcategoryComponent implements OnInit {

  constructor(private apiService: EcomService, private formbuilder: FormBuilder) { }

  myForm = this.formbuilder.group({
    CategoryName : ['',Validators.required]
  });

  ngOnInit(): void {
  }

  add()
  {
    if(this.myForm.status == 'VALID'){
      let category ={ 
        CategoryName : this.myForm.value.CategoryName 
      };

      this.apiService.addCategory(category).subscribe(response => {
        console.log(response);
      });

      alert('Submitted successfully');
    }

    else{
      alert('All fields are required.');
    }
  }

}
