import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ECom-Website.UI';
  payment: any;

  ngOnInit() {
    let logindetails = JSON.parse(localStorage.getItem('login')||"{}");

    if(logindetails.loginRole == "USER") {
      localStorage.setItem('isuser', 'true');
    }
    
    else {
      localStorage.setItem('isuser', 'false');
    }
  }
}
