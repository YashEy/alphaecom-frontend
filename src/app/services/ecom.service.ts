import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EcomService {

  baseurl: string = environment.baseurl;;
  
  constructor(private http: HttpClient) { }

  getProducts(){
    return this.http.get(this.baseurl + '/products');
  }
  
  getProductsByProductId(productId: any){
    return this.http.get(this.baseurl + '/products/' + productId);
  }

  addProduct(product: any){
    return this.http.post(this.baseurl + '/products/add', product);
  }

  deleteProduct(productId: any){
    return this.http.delete(this.baseurl + '/products/' + productId);
  }

  updateProduct(product: any){
    return this.http.put(this.baseurl + '/products/update',product);
  }

  getProductByCategoryId(categoryId: any){
    return this.http.get(this.baseurl + '/products/category/' + categoryId);
  }

  getCategories(){
    return this.http.get(this.baseurl + '/category');
  }

  getCategoryById(categoryId: any){
    return this.http.get(this.baseurl + '/category/' + categoryId);
  }

  addCategory(category: any){
    return this.http.post(this.baseurl + '/category/add', category); 
  }

  addCustomer(customer: any){
    return this.http.post(this.baseurl + '/customers/add', customer);
  }

  getCustomerByLoginId(loginId: any){
    return this.http.get(this.baseurl + '/customers/' + loginId);
  }

  getLoginByUserId(userid: any){
    return this.http.get(this.baseurl + '/login/' + userid);
  }

  addLogin(login: any){
    return this.http.post(this.baseurl + '/login/add', login);
  }

  getOrdersByCustId(custid: any){
    return this.http.get(this.baseurl + '/order/' + custid);
  }

  addOrder(order: any){
    return this.http.post(this.baseurl + '/order/add', order);
  }

  getPayments(){
    return this.http.get(this.baseurl + '/payment/' );
  }

  getPaymentById(paymentId: any){
    return this.http.get(this.baseurl + '/payment/' + paymentId); 
  }

  addPayment(payment: any){
    return this.http.post(this.baseurl + '/payment/add', payment);
  }
} 